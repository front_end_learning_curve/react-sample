# Start mock server

`npm run start`

Server port is configured running at 8080

To generate new DB

`npm run start-build`

# Here are some captured results

## User Dashboard

![Dashboard screen](./captured-screens/dashboard.png)

## User profile

![User profile screen](./captured-screens/user-profile.png)

## User Edit profile

![User edit profile screen](./captured-screens/user-edit.png)

![User edit profile screen](./captured-screens/user-edit2.png)