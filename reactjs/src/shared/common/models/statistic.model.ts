export class Statistic {
    
    public id!: number;
    public iconName!: string;
    public iconBgColor!: string;
    public iconColor!: string;
    public figure!: string;
    public desc?: string;
    public uom?: string;

    constructor(fields?: Partial<Statistic>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}