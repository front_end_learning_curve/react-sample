export class Role {
    public id!: number;
    public name!: string;

    constructor(fields?: Partial<Role>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

export class User {
    public id!: number;
    public name!: string;
    public jobTitle!: string;
    public avatar?: string;
    public roles!: Role[];
    public email!: string;
    public phone!: string;
    public twitter?: string;
    public location!: string;
    public biography?: string;

    constructor(fields?: Partial<User>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}