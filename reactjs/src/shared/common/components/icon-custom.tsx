import React, { Component } from "react";

import { MailOutlined, MessageOutlined, CalendarOutlined } from '@ant-design/icons';

interface IconCustomProps {
    // custom only
    tag: string;
    style: any;
}

export class IconCustom extends Component<IconCustomProps, {}> {

    components = {
        mailOutlined: MailOutlined,
        messageOutlined: MessageOutlined,
        calendarOutlined: CalendarOutlined
    };

    render() {
        const TagName = this.components[this.props.tag || 'mailOutlined'];
        return <TagName style={this.props.style} />
     }
}