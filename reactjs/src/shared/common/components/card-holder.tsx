import React, { Component } from 'react';

import { Card, Avatar } from 'antd';

import './card-holder.css';

interface CardHolderProps {
    icon: React.ReactNode;
    bgColor: string;
    figure: string;
    desc?: string;
    uom?: string;
}

export default class CardHolder extends Component<CardHolderProps, {}> {

    render() {

        return (
            <Card bordered={false}>
                <Avatar className="card-icon" style={{backgroundColor: this.props.bgColor}} icon={this.props.icon} />
                <div className="card-desc">
                    <div className="figure">{this.props.figure}{this.props.uom}</div>
                    <div>{this.props.desc}</div>
                </div>
            </Card>
        );
    }
}