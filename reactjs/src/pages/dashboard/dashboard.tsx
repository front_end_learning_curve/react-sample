import React, { Component } from 'react';

import { Table } from 'antd';
import axios from "axios";
import { Link } from 'react-router-dom';

import CardHolder from '../../shared/common/components/card-holder';
import {
    SERVER_API_URL,
    STATISTIC_URL,
    AUTHORS_URL
} from '../../app.endpoints';
import { Statistic } from '../../shared/common/models/statistic.model';
import { IconCustom } from '../../shared/common/components/icon-custom';
import { User } from '../../shared/common/models/user.model';


import './dashboard.css';

const columns = [
    {
        title: 'ID',
        dataIndex: 'id',
        width: 55,
    },
    {
        title: 'Name',
        dataIndex: 'name',
    },
    {
        title: 'Job Title',
        dataIndex: 'jobTitle',
    },
    {
        title: 'Email',
        dataIndex: 'email',
    },
    {
        title: '',
        render: (text, record) => (
            <span>
                <Link to={`/authors/${record.id}`}>Open</Link>
            </span>
        )
    }
];


interface DashboardState {
    selectedRowKeys: any,
    statistics: Statistic[],
    authors: User[]
}

export default class Dashboard extends Component<{}, DashboardState> {

    constructor(props) {

        super(props);

        this.state = {
            selectedRowKeys: [],
            statistics: [],
            authors: []
        }

    }



    componentDidMount() {
        // fetch statistics
        axios
            .get(`${SERVER_API_URL}${STATISTIC_URL}`)
            .then(response => {
                this.setState({
                    statistics: response.data
                })
            })
            .catch(error => {
                console.log(error);
            });

        // fetch authors
        axios
            .get(`${SERVER_API_URL}${AUTHORS_URL}`)
            .then(response => {
                this.setState({
                    authors: response.data
                });
                // console.log(response.data);
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {

        return (
            <React.Fragment>
                <div className="card-group">
                    {this.state.statistics.map(statistic => {
                        return (
                            <CardHolder key={statistic.id} icon={<IconCustom tag={statistic.iconName} style={{ color: statistic.iconColor }} />} bgColor={statistic.iconBgColor} figure={statistic.figure} desc={statistic.desc} uom={statistic.uom} />
                        );
                    })}
                </div>
                <div>
                    <Table rowKey="id" columns={columns} dataSource={this.state.authors} scroll={{ y: 400 }} />
                </div>
            </React.Fragment>
        );
    }
}