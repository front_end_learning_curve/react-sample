import React, { useEffect, useState } from 'react';

import { useParams, useHistory } from 'react-router-dom';

import {
    Form,
    Input,
    Button,
    Select,
    Row,
    Col,
    Typography,
    notification,
} from 'antd';
import axios from "axios";

import {
    User,
    Role
} from '../../shared/common/models/user.model';
import {
    SERVER_API_URL,
    GET_ONE_AUTHOR_URL,
    ROLES_URL,
    AUTHORS_URL
} from '../../app.endpoints';

const { Option } = Select;
const { TextArea } = Input;
const { Title } = Typography;

const UserEdit = (props) => {

    let history = useHistory();
    const [user, setUser] = useState<User | any>(null);
    const [roles, setRoles] = useState<Role[] | any>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const { id } = useParams();

    useEffect(() => {

        axios
            .get(`${SERVER_API_URL}${GET_ONE_AUTHOR_URL.replace(":id", `${id}`)}`)
            .then(response => {
                setUser(response.data);
                setLoading(false);
            })
            .catch(error => {
                console.log(error);
                setLoading(false);
            });

        axios
            .get(`${SERVER_API_URL}${ROLES_URL}`)
            .then(response => {
                setRoles(response.data);
            })
            .catch(error => {
                console.log(error);
            });

    }, []);

    function onFinish(values) {

        const updatedUser = {
            ...user,
            values,
        };

        // compare pre user and current
        if (JSON.stringify(updatedUser) === JSON.stringify(user)) {

            return;
        }

        // show loading
        setLoading(true);

        // update user data
        setUser({
            ...user,
            values
        });

        axios
            .put(`${SERVER_API_URL}${AUTHORS_URL}/${user.id}`, user)
            .then(response => {

                setLoading(false);

                if (response.status === 200) {
                    showNotification("success", "Update successfully!");
                }
            })
            .catch(error => {

                setLoading(false);
                console.log(error);
                showNotification("error", `Update failure! ${error.message}`);
            })

        
    };

    function showNotification(type: string, message: string) {

        notification[type]({
            message: 'Edit Profile',
            description: message,
        });
    }

    function initilizeRoles(userRoles: Role[]) {

        if (userRoles == null || userRoles === undefined || userRoles.length === 0) {

            return [];
        }

        return userRoles.map(role => role.id);
    }

    function onChangeRoles(values: number[]) {
        console.log(`selected ${values}`);

        if (values.length === 0) {

            setUser({
                ...user,
                roles: []
            });

        } else {

            let updatedRoles: Role[] = [];
            for (let i = 0; i < values.length; i++) {
                
                const index = roles.findIndex(role => role.id === values[i]);
                if (index > -1) {
                    updatedRoles.push(roles[index]);
                }
            }

            setUser({
                ...user,
                roles: updatedRoles
            });
            console.log(updatedRoles);
        }
    }

    function onChangeBiography({ target: { value } }) {
        setUser({
            ...user,
            biography: value
        });
    }

    function goBack() {

        history.push("/");
    }

    return (
        <div className="user-edit">
            <Title level={3}>Edit Profile</Title>
            <br />
            {user && roles &&
                <Form
                    onFinish={onFinish}
                    initialValues={{
                        ...user
                    }}
                >
                    <div className="user-actions">
                        <Button loading={loading} type="primary" htmlType="submit">Save</Button>
                        <Button onClick={goBack}>Back</Button>
                    </div>

                    <Row gutter={[60, 16]}>
                        <Col span={12}>
                            {/* ROLE */}
                            <h4 >ROLE</h4>
                            <Select
                                mode="multiple"
                                style={{ width: '100%' }}
                                placeholder="Select one role"
                                defaultValue={initilizeRoles(user.roles)}
                                onChange={onChangeRoles}
                                optionLabelProp="label"
                            >
                                {roles.map(role => {
                                    return (
                                        <Option key={role.id} value={role.id} label={role.name}>
                                            <div className="demo-option-label-item">
                                                <span role="img" aria-label={role.name}> {role.name}</span>
                                            </div>
                                        </Option>
                                    );
                                })}


                            </Select>
                        </Col>
                        <Col span={12}>
                            {/* Email  */}
                            <h4 >EMAIL</h4>
                            <Form.Item
                                name="email"
                                rules={[{ required: true, message: 'Please input your email!' }]}
                            >
                                <Input
                                    type="email"
                                    placeholder="Email"
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={[60, 16]}>
                        <Col span={12}>
                            {/* Phone */}
                            <h4 >PHONE</h4>
                            <Form.Item
                                name="phone"
                                rules={[{ required: true, message: 'Please input your phone!' }]}
                            >
                                <Input
                                    type="text"
                                    placeholder="Phone"

                                />
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            {/* TWITTER */}
                            <h4 >TWITTER</h4>
                            <Form.Item
                                name="twitter"
                                rules={[{ required: true, message: 'Please input your twitter!' }]}
                            >
                                <Input
                                    type="text"
                                    placeholder="Twitter"
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={[60, 16]}>
                        <Col span={12}>
                            {/* Location */}
                            <h4 >LOCATION</h4>
                            <Form.Item
                                name="location"
                                rules={[{ required: true, message: 'Please input your location!' }]}
                            >
                                <Input
                                    type="text"
                                    placeholder="Location"
                                />
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            {/* BIOGRAPHY */}
                            <h4 >BIO</h4>
                            <TextArea name="biography" rows={3} placeholder="Biography" value={user.biography} onChange={onChangeBiography} />
                        </Col>
                    </Row>

                </Form>

            }
        </div>

    );
}

export default UserEdit;