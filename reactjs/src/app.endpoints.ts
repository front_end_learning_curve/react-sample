export const SERVER_API_URL = 'http://localhost:8080';

export const STATISTIC_URL = '/statistics';

export const AUTHORS_URL = '/authors';

export const GET_ONE_AUTHOR_URL = '/authors/:id';

export const ROLES_URL = '/roles';