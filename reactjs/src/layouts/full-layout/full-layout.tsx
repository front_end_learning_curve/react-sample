import React, { Component } from 'react';

import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import {
    Layout,
    Breadcrumb
} from 'antd';

import {
    MenuUnfoldOutlined,
    MenuFoldOutlined
} from '@ant-design/icons';

import Header from './header/header';
import QuickSide from './sidenav/quickside';
import SideNav from './sidenav/sidenav';    
import Dashboard from '../../pages/dashboard/dashboard';

import './full-layout.css';
import UserEdit from '../../pages/user/user-edit';

interface FullLayoutState {
    collapsed: boolean
}

export default class FullLayout extends Component<{}, FullLayoutState> {

    constructor(props) {

        super(props);

        this.state = {
            collapsed: false
        }
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed
        });
    };

    render() {

        return (
            <Layout>
                <Router>
                    <SideNav collapsed={this.state.collapsed} />

                    <Layout className="site-layout">

                        <Header collapsed={this.state.collapsed}>
                            {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                                className: 'trigger',
                                onClick: this.toggle
                            })}
                        </Header>

                        {/* <Breadcrumb style={{ margin: '16px 0', padding: '0 24px' }}>
                            <Breadcrumb.Item>Home</Breadcrumb.Item>
                            <Breadcrumb.Item>List</Breadcrumb.Item>
                            <Breadcrumb.Item>App</Breadcrumb.Item>
                        </Breadcrumb> */}
                        <Layout.Content
                            style={{
                                margin: '12px 16px 24px 50px',
                                padding: 24,
                                height: '100vh'
                            }}
                        >
                                <Switch>
                                    <Route path="/authors/:id">
                                        <UserEdit />
                                    </Route>
                                    <Route path="/" >
                                        <Dashboard />
                                    </Route>
                                </Switch>
                            </Layout.Content>
                        <Layout.Footer style={{ textAlign: 'center' }}>Ant Design ©2020 Cusmized by Ocean Nguyen</Layout.Footer>
                    </Layout>
                    <QuickSide />
                </Router>
            </Layout>

        );
    }

}