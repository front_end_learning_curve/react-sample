import React, { Component } from 'react';

import { Button, Tooltip, Avatar, Layout } from 'antd';
import { BellOutlined, MailOutlined } from '@ant-design/icons';

import './header.css';

interface HeaderProps {
    collapsed: boolean;
}

export default class Header extends Component<HeaderProps, {}> {

    render() {

        return (
            <Layout.Header className="site-layout-background" style={{ padding: 10 }}>
                {this.props.children}
                <Tooltip title="avatar" className="pull-right nav-item avatar">
                    <Button shape="circle"><Avatar src="https://avatarfiles.alphacoders.com/880/thumb-88081.jpg" /></Button>
                </Tooltip>
                <Tooltip title="mail" className="pull-right nav-item">
                    <Button shape="circle" icon={<MailOutlined />} />
                </Tooltip>
                <Tooltip title="notification" className="pull-right nav-item">
                    <Button shape="circle" icon={<BellOutlined />} />
                </Tooltip>
            </Layout.Header>
        );
    }
}