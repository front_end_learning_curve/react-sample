import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import {
    Layout,
    Button,
    Avatar,
    Typography,
    Space,
    Divider,
    Badge
} from 'antd';
import {
    NodeExpandOutlined,
    NodeCollapseOutlined,
} from '@ant-design/icons';
import axios from 'axios';

import { User } from '../../../shared/common/models/user.model';

import './quickside.css';
import {
    SERVER_API_URL,
    GET_ONE_AUTHOR_URL
} from '../../../app.endpoints';


interface QuickSideState {
    collapsed: boolean;
}

const { Title, Text } = Typography;

export default class QuickSide extends Component<{}, QuickSideState> {

    author: User;
    defaultAuthorId = 1;

    constructor(props) {

        super(props);

        this.state = {
            collapsed: true
        };

        this.author = new User();
    }

    componentDidMount() {

        axios
            .get(`${SERVER_API_URL}${GET_ONE_AUTHOR_URL.replace(":id", `${this.defaultAuthorId}`)}`)
            .then(response => {
                // console.log(response.data);
                this.author = response.data;
            })
            .catch(error => {
                console.log(error);
            })
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed
        });
    };

    onFinish = values => {
        console.log('Received values of form: ', values);
    };

    handleChange(value) {
        console.log(`selected ${value}`);
    }

    render() {
        return (
            <Layout.Sider collapsed={this.state.collapsed} width={300} theme="light">
                {React.createElement(this.state.collapsed ? NodeExpandOutlined : NodeCollapseOutlined
                    , {
                        className: 'trigger',
                        onClick: this.toggle
                    })}

                <div className="quick-side-wrapper" style={{ display: this.state.collapsed && this.author ? 'none' : 'block' }}>
                    <div className="user-profile">
                        <Badge>
                            <Avatar size={120} src={this.author.avatar} />
                        </Badge>
                        <br />
                        <Title level={3} className="user-name" >{this.author.name}</Title>
                        <Text type="secondary">{this.author.jobTitle}</Text>
                        <br /><br />
                        <Space size="small">
                            <Button type="primary"><Link to={`/authors/${this.author.id}`}>Edit Profile</Link></Button>
                            <Button type="primary">Change Status</Button>
                        </Space>
                    </div>
                    <Divider />
                    <div className="user-desc">
                        <h4 >ROLE</h4>
                        <p>{this.author.roles?.map(role => role.name).join(", ")}</p>
                        <h4 >EMAIL</h4>
                        <p>{this.author.email}</p>
                        <h4 >PHONE</h4>
                        <p>{this.author.phone}</p>
                        <h4 >TWITTER</h4>
                        <p>{this.author.twitter}</p>
                        <h4 >LOCATION</h4>
                        <p>{this.author.location}</p>
                        <h4 >BIO</h4>
                        <p>{this.author.biography}</p>
                    </div>
                </div>
            </Layout.Sider>
        );
    }
}