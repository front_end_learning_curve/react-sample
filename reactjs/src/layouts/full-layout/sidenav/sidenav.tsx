import React, { Component } from 'react';

import {
    Link
} from "react-router-dom";
import { Layout, Menu } from 'antd';
import {
    UserOutlined,
    LaptopOutlined,
    NotificationOutlined,
    AppstoreOutlined
} from '@ant-design/icons';

interface SideNavProps {
    collapsed: boolean;
}

interface SideNavState {
    collapsed: boolean;
}

export default class SideNav extends Component<SideNavProps, SideNavState> {

    render() {

        return (
            <Layout.Sider collapsed={this.props.collapsed} width={200} theme="light">
                <div className="logo">
                    <Link to="/">
                        <img className={this.props.collapsed ? "small-view" : "full-view"} src={this.props.collapsed ? "/logo_resize.png" : "/img_ocean_logo.png"} alt="logo" />
                    </Link>                    
                </div>
                
                    <Menu
                        theme="light"
                        mode="inline"
                        defaultSelectedKeys={['0']}
                        style={{ height: '100%', borderRight: 0 }}
                    >
                        <Menu.Item key="0" icon={<AppstoreOutlined />}>
                            <Link to="/">Dashboard</Link>
                        </Menu.Item>
                        <Menu.SubMenu key="sub1" icon={<UserOutlined />} title="subnav 1">
                            <Menu.Item key="1">option1</Menu.Item>
                            <Menu.Item key="2">option2</Menu.Item>
                            <Menu.Item key="3">option3</Menu.Item>
                            <Menu.Item key="4">option4</Menu.Item>
                        </Menu.SubMenu>
                        <Menu.SubMenu key="sub2" icon={<LaptopOutlined />} title="subnav 2">
                            <Menu.Item key="5">option5</Menu.Item>
                            <Menu.Item key="6">option6</Menu.Item>
                            <Menu.Item key="7">option7</Menu.Item>
                            <Menu.Item key="8">option8</Menu.Item>
                        </Menu.SubMenu>
                        <Menu.SubMenu key="sub3" icon={<NotificationOutlined />} title="subnav 3">
                            <Menu.Item key="9">option9</Menu.Item>
                            <Menu.Item key="10">option10</Menu.Item>
                            <Menu.Item key="11">option11</Menu.Item>
                            <Menu.Item key="12">option12</Menu.Item>
                        </Menu.SubMenu>
                    </Menu>

            </Layout.Sider>
        );  
    }
}