
import React from 'react';

import FullLayout from './layouts/full-layout/full-layout';

import 'antd/dist/antd.css';
import './App.css';

function App() {
  
  return (
    <React.Fragment>

      <FullLayout />
      
    </React.Fragment>
  );
}


export default App;
