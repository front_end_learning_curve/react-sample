const faker = require("faker");
const _ = require("lodash");
const moment = require("moment");

module.exports = function () {

    return {
        authors: _.times(100, (id) => {
            return {
                id: id + 1,
                name: faker.name.findName(),
                jobTitle: faker.name.jobTitle(),
                avatar: faker.internet.avatar(),
                roles: [{id: 1, name: "User"}],                
                email: faker.internet.email(),
                phone: faker.phone.phoneNumber(),
                twitter: faker.internet.email(),   
                location: faker.address.streetAddress(),            
                biography: faker.lorem.sentences()
            }
        })
    };
}